import java.io.IOException;

/**
 * Created by davidbe on 11/1/2017.
 */
public class Main {
    public static void main(String[] args) {
        if (args.length != 1){
            throw new IllegalArgumentException("Please provide full csv file path");
        }
        try{
            Worker worker = new Worker(args[0]);
            worker.work();
            worker.printResults();
        }
        catch(IOException|InterruptedException e){
            e.printStackTrace();
        }
    }
}
