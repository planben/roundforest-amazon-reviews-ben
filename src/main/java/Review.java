/**
 * Created by davidbe on 11/2/2017.
 */
public class Review {
    String productId;
    String profileName;
    String summary;
    String text;
    public Review() {

    }

    public Review(String productId, String profileName, String summary, String text) {
        this.productId = productId;
        this.profileName = profileName;
        this.summary = summary;
        this.text = text;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProfileName() {
        return profileName;
    }

    public void setProfileName(String profileName) {
        this.profileName = profileName;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
