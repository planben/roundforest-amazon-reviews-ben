import com.google.common.base.Splitter;
import com.google.common.collect.Lists;
import jdk.nashorn.internal.ir.debug.ObjectSizeCalculator;

import java.io.*;
import java.util.*;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Function;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by davidbe on 11/1/2017.
 */
public class Worker {
    String path;
    Map<String,Long> userReviews = new HashMap<>();
    Map<String,Long> itemReviews = new HashMap<>();
    Map<String,Long> mostUsedWords = new HashMap<>();
    List<String> allWords;
    private static Splitter regexpSplitter = Splitter.on(Pattern.compile(",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)"));
    private static Splitter wordSplitter = Splitter.on(" ").omitEmptyStrings().trimResults();
    private static final int REVIEWS_CHUNK_LIMIT = 50000;
    ExecutorService service = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Worker(String path){
        setPath(path);
    }

    public void work() throws IOException, InterruptedException {
        System.out.println("freeMemory : " + Runtime.getRuntime().freeMemory());
        System.out.println("totalMemory: " + Runtime.getRuntime().totalMemory());
        System.out.println("maxMemory : " + Runtime.getRuntime().maxMemory());
        File inputF = new File(getPath());
        InputStream inputFS = new FileInputStream(inputF);
        BufferedReader br = new BufferedReader(new InputStreamReader(inputFS));
        long before = System.currentTimeMillis();
        long linesToSkip = 1;
        List<Review> reviews = br.lines().skip(linesToSkip).limit(REVIEWS_CHUNK_LIMIT).map(mapToItem).collect(Collectors.toList());
        while (reviews != null && !reviews.isEmpty()){
            long after = System.currentTimeMillis();
            System.out.println("reading file took : " + (after - before) + " milliseconds");
            System.out.println(reviews.size() + " reviews in the current chunk");
            System.out.println("current reviews chunk size: " + ObjectSizeCalculator.getObjectSize(reviews));
            System.out.println("freeMemory : " + Runtime.getRuntime().freeMemory());
            System.out.println(Runtime.getRuntime().availableProcessors() + " total processors");
            br.close();

            // convert to map (group by profileName, count(*))
            before = System.currentTimeMillis();
            userReviews = mergeMaps(userReviews,reviews.stream().collect(Collectors.groupingBy(o -> o.getProfileName(),Collectors.counting())));
            after = System.currentTimeMillis();
            System.out.println("grouping by profile name took : " + (after - before) + " milliseconds");
            System.out.println("freeMemory : " + Runtime.getRuntime().freeMemory());

            // convert to map (group by productId  , count(*))
            before = System.currentTimeMillis();
            itemReviews = mergeMaps(itemReviews,reviews.stream().collect(Collectors.groupingBy(o -> o.getProductId(),Collectors.counting())));
            after = System.currentTimeMillis();
            System.out.println("grouping by product id took : " + (after - before) + " milliseconds");
            System.out.println("freeMemory : " + Runtime.getRuntime().freeMemory());

            before = System.currentTimeMillis();
            allWords = new LinkedList<>();
            List<List<Review>> allPartitionedReviews = Lists.partition(reviews, reviews.size() / Runtime.getRuntime().availableProcessors());
            System.out.println(allPartitionedReviews.size() + " total chunks of reviews");
            List<ReviewCallable> callables = new ArrayList<>(allPartitionedReviews.size());
            allPartitionedReviews.stream().forEach(reviews1 -> callables.add(new ReviewCallable(reviews1)));
            service.invokeAll(callables);
            after = System.currentTimeMillis();
            System.out.println("freeMemory : " + Runtime.getRuntime().freeMemory());
            System.out.println("collecting words took : " + (after - before) + " milliseconds");

            before = System.currentTimeMillis();

            after = System.currentTimeMillis();
            System.out.println("grouping by words took : " + (after - before) + " milliseconds");
            mostUsedWords = mergeMaps(mostUsedWords,allWords.stream().collect(Collectors.groupingBy(o -> o,Collectors.counting())));
            allWords.clear();
            System.out.println("freeMemory : " + Runtime.getRuntime().freeMemory());

            inputFS = new FileInputStream(inputF);
            br = new BufferedReader(new InputStreamReader(inputFS));
            linesToSkip += REVIEWS_CHUNK_LIMIT;
            reviews = br.lines().skip(linesToSkip).limit(REVIEWS_CHUNK_LIMIT).map(mapToItem).collect(Collectors.toList());
         }

        // sorting and keeping first 1000
        userReviews = sortAndLimitMaps(userReviews,1000);
        itemReviews = sortAndLimitMaps(itemReviews,1000);
        mostUsedWords = sortAndLimitMaps(mostUsedWords,1000);
    }

    private Map<String, Long> sortAndLimitMaps(Map<String, Long> map, int limit) {
        return map.entrySet().stream()
                .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder())).limit(limit)
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue,
                        (oldValue, newValue) -> oldValue, LinkedHashMap::new));
    }

    public void printResults(){
        System.out.println("MOST COMMENTING USERS : ");
        //userReviews.entrySet().forEach(System.out::println);
        sortMapAlphabetically(userReviews).entrySet().forEach(System.out::println);
        System.out.println("**************************************");

        System.out.println("MOST COMMENTED ITEMS : ");
        //itemReviews.entrySet().forEach(System.out::println);
        sortMapAlphabetically(itemReviews).entrySet().forEach(System.out::println);
        System.out.println("**************************************");

        System.out.println("MOST USED WORDS : ");
        //mostUsedWords.entrySet().forEach(System.out::println);
        sortMapAlphabetically(mostUsedWords).entrySet().forEach(System.out::println);
        System.out.println("**************************************");
    }

        private Function<String, Review> mapToItem = (line) -> {
            List<String> strings = regexpSplitter.splitToList(line);
            Review review = new Review();
            review.setProductId(strings.get(1));
            review.setProfileName(strings.get(3));
            review.setSummary(strings.get(8).toLowerCase());
            review.setText(strings.get(9).toLowerCase());
            return review;
        };

    private Map<String, Long> mergeMaps(Map<String, Long> existingMap, Map<String, Long> newMap) {
        return Stream.of(existingMap, newMap)
                .map(Map::entrySet)
                .flatMap(Collection::stream)
                .collect(
                        Collectors.toMap(
                                Map.Entry::getKey,
                                Map.Entry::getValue,
                                (oldValue,newValue) -> oldValue + newValue
                        ));
    }

    private Map<String, Long> sortMapAlphabetically(Map<String, Long> map) {
        return map.entrySet().stream()
                .sorted(Map.Entry.comparingByKey())
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue,
                        (oldValue, newValue) -> oldValue, LinkedHashMap::new));
    }

    private class ReviewCallable implements Callable<List<Review>>{

        private List<Review> reviews;

        public ReviewCallable(List<Review> reviews){
            this.reviews = reviews;
        }

        @Override
        public List<Review> call() throws Exception {
            long before = System.currentTimeMillis();
            for (Review review : reviews) {
                allWords.addAll(wordSplitter.splitToList(review.getSummary()));
                allWords.addAll(wordSplitter.splitToList(review.getText()));
            }
            long after = System.currentTimeMillis();
            System.out.println("chunk took " + (after - before) + " milliseconds");
            return reviews;
        }
    }
}
